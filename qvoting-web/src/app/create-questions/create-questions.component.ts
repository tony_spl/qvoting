import { Component, OnInit } from '@angular/core';
import { Question } from 'src/models/question';

@Component({
  selector: 'app-create-questions',
  templateUrl: './create-questions.component.html',
  styleUrls: ['./create-questions.component.scss']
})
export class CreateQuestionsComponent implements OnInit {
  private emptyQuestion = { title: '', subtitle: '' };
  public questions: Question[] = [this.emptyQuestion];

  constructor() { }

  ngOnInit() {
  }

  addQuestion() {
    this.questions.push(this.emptyQuestion);
  }
}
