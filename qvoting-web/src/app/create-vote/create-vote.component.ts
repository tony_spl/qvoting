import { Component, OnInit, Input } from '@angular/core';
import { Poll } from 'src/models/poll';

@Component({
  selector: 'app-create-vote',
  templateUrl: './create-vote.component.html',
  styleUrls: ['./create-vote.component.scss']
})
export class CreateVoteComponent implements OnInit {
  public poll: Poll = {
    title: '',
    public: true,
    description: ''
  };

  constructor() { }

  ngOnInit() {
  }

}
