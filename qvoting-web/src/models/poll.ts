export interface Poll {
    title: string;
    description: string;
    public: boolean;
    questions?: number;
}
