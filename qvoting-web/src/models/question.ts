export interface Question {
    title: string;
    subtitle: string;
    numVotes?: number;
}
